import * as Blockly from 'blockly/core';

const MAX_HUE = 120;

//红色LED 模块
export const MAX_led_R = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_MAX_LED_R)
            .appendField(new Blockly.FieldImage(require("../media/led_r.png"), 63, 40));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};

//黄色LED 模块
export const MAX_led_Y = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_MAX_LED_Y)
            .appendField(new Blockly.FieldImage(require("../media/led_y.png"), 63, 40));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};

//绿色LED 模块
export const MAX_led_G = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_MAX_LED_G)
            .appendField(new Blockly.FieldImage(require("../media/led_g.png"), 63, 40));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};

//按键传感器
export const MAX_button = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_MAX_BUTTON)
            .appendField(new Blockly.FieldImage(require("../media/button.png"), 63, 40));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setOutput(true, Boolean);
        this.setInputsInline(true);
        this.setTooltip('');
    }
};


//声音传感器
export const MAX_sound = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_MAX_SOUND)
            .appendField(new Blockly.FieldImage(require("../media/sound.png"), 63, 40));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setInputsInline(true);
        this.setOutput(true, Number);
        this.setTooltip('');
    }
};
//光线传感器
export const MAX_light = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_MAX_LIGHT)
            .appendField(new Blockly.FieldImage(require("../media/light.png"), 63, 40));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setInputsInline(true);
        this.setOutput(true, Number);
        this.setTooltip('');
    }
};

export const ke_w_buzzer2 = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_ke_BUZZER2)
            .appendField(new Blockly.FieldImage(require("../media/buzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendValueInput('FREQUENCY')
            .setCheck(Number)
            //.setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_FREQUENCY);
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};
export const ke_w_buzzer3 = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_ke_BUZZER2)
            .appendField(new Blockly.FieldImage(require("../media/buzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendValueInput('FREQUENCY')
            .setCheck(Number)
            //.setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_FREQUENCY);
        this.appendValueInput('DURATION')
            .setCheck(Number)
            //.setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_DURATION);
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

//////////////////蜂鸣器//////////////////
var TONE_NOTES = [
    ["NOTE_C3", "131"], ["NOTE_D3", "147"], ["NOTE_E3", "165"], ["NOTE_F3", "175"], ["NOTE_G3", "196"], ["NOTE_A3", "220"], ["NOTE_B3", "247"],
    ["NOTE_C4", "262"], ["NOTE_D4", "294"], ["NOTE_E4", "330"], ["NOTE_F4", "349"], ["NOTE_G4", "392"], ["NOTE_A4", "440"], ["NOTE_B4", "494"],
    ["NOTE_C5", "532"], ["NOTE_D5", "587"], ["NOTE_E5", "659"], ["NOTE_F5", "698"], ["NOTE_G5", "784"], ["NOTE_A5", "880"], ["NOTE_B5", "988"]
];

export const ke_tone01 = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown(TONE_NOTES), 'STAT');
        this.setOutput(true, Number);
    }
};

export const ke_buzzer = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_ke_BUZZER2)
            .appendField(new Blockly.FieldImage(require("../media/buzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendValueInput('FREQUENCY')
            .setCheck(Number)
            //.setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.kids_tone);

        //this.appendValueInput('DURATION')
        //.setCheck(Number)
        //.setAlign(Blockly.ALIGN_RIGHT)
        //.appendField(Blockly.Msg.kids_beat);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.kids_beat)
            .appendField(new Blockly.FieldDropdown([["1/8", "125"], ["3/8", "375"], ["1/4", "250"], ["3/4", "750"], ["1/2", "500"], ["1", "1000"]]), 'beat1');

        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};


///////////music////////////////////
export const ke_music = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.kids_play_tone)
            .appendField(new Blockly.FieldImage(require("../media/buzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            //.appendField(new Blockly.FieldDropdown([["Birthday", "Birthday"],["City of Sky", "City of Sky"],["Ode to Joy", "Ode to Joy"]]), 'play');
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.kids_Ode_to_joy, "Ode to Joy"], [Blockly.Msg.kids_birthday, "Birthday"]]), 'play');
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

////////////////////关闭蜂鸣器////////////////////////
export const ke_notone = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.kids_notone)
            .appendField(new Blockly.FieldImage(require("../media/buzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};



//RGB彩灯显示模块  初始化
export const MAX_rgb_init = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_RGB)
            .appendField(new Blockly.FieldImage(require("../media/RGB.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_PIN);
        this.appendValueInput("LEDCOUNT")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_RGB_COUNT);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('');
    }
};
//RGB亮度调节
export const MAX_rgb_brightness = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_RGB)
            .appendField(new Blockly.FieldImage(require("../media/RGB.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_PIN);
        this.appendValueInput("NUM")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_brightness);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('');
    }
};
//RGB彩灯单独像素点设置
export const MAX_rgb = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_RGB)
            .appendField(new Blockly.FieldImage(require("../media/RGB.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_PIN);
        this.appendValueInput("_LED_")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_RGB_NUM);
        this.appendValueInput("RVALUE")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_RGB_R);
        this.appendValueInput("GVALUE")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_RGB_G);
        this.appendValueInput("BVALUE")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_RGB_B);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('');
    }
};
//RGB彩灯  选色块  设置颜色
export const MAX_rgb2 = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_RGB)
            .appendField(new Blockly.FieldImage(require("../media/RGB.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_PIN);
        this.appendValueInput("_LED_")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_RGB_NUM);
        this.appendDummyInput("")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldColour("#ff0000"), "RGB_LED_COLOR");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
//RGB彩灯  七彩变换切换
export const MAX_rgb3 = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_RGB)
            .appendField(new Blockly.FieldImage(require("../media/RGB.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_PIN);
        this.appendValueInput("NUM")
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_init_brightness);
        this.appendValueInput("WAIT")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_TIME_FOR_CHANGE_COLOUR);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
//RGB彩灯 七彩循环切换
export const MAX_rgb4 = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_RGB)
            .appendField(new Blockly.FieldImage(require("../media/RGB.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_PIN);
        this.appendValueInput("NUM")
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_init_brightness);
        this.appendValueInput("WAIT")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_TIME_FOR_LOOP_COLOUR);
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};



export const MAX_88display_Matrix_blink_rate = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOW)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendValueInput('Blink_rate')
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_ESP32_JS_MONITOR_SET_BLINK_RATE);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip("取值范围0/1/2");
    }
};

export const MAX_88display_Matrix_brightness = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOW)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendValueInput("Brightness")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_BRIGHTNESS);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip("取值范围0~15");
    }
};

//执行器_点阵屏显示_画点变量
var MAX_88display_DrawPixel_NUM = [
    [Blockly.Msg.MIXLY_4DIGITDISPLAY_ON, "1"],
    [Blockly.Msg.MIXLY_4DIGITDISPLAY_OFF, "0"]
];

//执行器_点阵屏显示_画点显示
export const MAX_88display_Matrix_POS = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOW)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendValueInput('XVALUE')
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_X);
        this.appendValueInput("YVALUE")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_Y);
        this.appendDummyInput("")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT)
            .appendField(new Blockly.FieldDropdown(MAX_88display_DrawPixel_NUM), "DrawPixel_TYPE");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT_TOOLTIP);
    }
};


//HT16K33点阵 绘制直线
export const MAX_88display_Matrix_drawLine = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_drawLine)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendValueInput('X0VALUE').setCheck(Number).appendField("X0");
        this.appendValueInput("Y0VALUE").setCheck(Number).appendField("Y0");
        this.appendValueInput('X1VALUE').setCheck(Number).appendField("X1");
        this.appendValueInput("Y1VALUE").setCheck(Number).appendField("Y1");

        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT)
            .appendField(new Blockly.FieldDropdown(MAX_88display_DrawPixel_NUM), "DrawPixel_TYPE");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT_TOOLTIP);
    }
};
//HT16K33点阵 画圈显示
export const MAX_88display_Matrix_drawCircle = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_drawCircle)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown([['hollow', '0'], ['filling', '1']]), "drawCircle_TYPE");
        this.appendValueInput('XVALUE')
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_X);
        this.appendValueInput("YVALUE")
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_Y);
        this.appendValueInput("RVALUE")
            .setCheck(Number)
            .appendField("radius");
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT)
            .appendField(new Blockly.FieldDropdown(MAX_88display_DrawPixel_NUM), "DrawPixel_TYPE");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT_TOOLTIP);
    }
};

//HT16K33点阵 画矩形显示
export const MAX_88display_Matrix_drawRect = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_drawRect)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown([['hollow', '0'], ['filling', '1']]), "drawRect_TYPE");
        this.appendValueInput('XVALUE')
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_X);
        this.appendValueInput("YVALUE")
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_Y);
        this.appendValueInput('WVALUE')
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_X + 'width');
        this.appendValueInput("HVALUE")
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_Y + 'length');
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT)
            .appendField(new Blockly.FieldDropdown(MAX_88display_DrawPixel_NUM), "DrawPixel_TYPE");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT_TOOLTIP);
    }
};

//HT16K33点阵 绘制圆角矩形显示
export const MAX_88display_Matrix_drawRoundRect = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_drawRoundRect)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown([['hollow', '0'], ['filling', '1']]), "drawRoundRect_TYPE");
        this.appendValueInput('XVALUE')
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_X);
        this.appendValueInput("YVALUE")
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_Y);
        this.appendValueInput('WVALUE')
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_X + 'width');
        this.appendValueInput("HVALUE")
            .setCheck(Number)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_Y + 'length');
        this.appendValueInput("RVALUE")
            .setCheck(Number)
            .appendField("radius");
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT)
            .appendField(new Blockly.FieldDropdown(MAX_88display_DrawPixel_NUM), "DrawPixel_TYPE");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT_TOOLTIP);
    }
};

//HT16K33点阵 绘制三角形显示
export const MAX_88display_Matrix_drawTriangle = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_drawTriangle)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown([['hollow', '0'], ['filling', '1']]), "drawTriangle_TYPE");
        this.appendValueInput('X0VALUE')
            .setCheck(Number)
            .appendField("X0");
        this.appendValueInput("Y0VALUE")
            .setCheck(Number)
            .appendField("Y0");
        this.appendValueInput('X1VALUE')
            .setCheck(Number)
            .appendField("X1");
        this.appendValueInput("Y1VALUE")
            .setCheck(Number)
            .appendField("Y1");
        this.appendValueInput('X2VALUE')
            .setCheck(Number)
            .appendField("X2");
        this.appendValueInput("Y2VALUE")
            .setCheck(Number)
            .appendField("Y2");
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT)
            .appendField(new Blockly.FieldDropdown(MAX_88display_DrawPixel_NUM), "DrawPixel_TYPE");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOWPOINT_TOOLTIP);
    }
};




//执行器_点阵屏显示_左右对称
var MAX_88display_Rotation_NUM = [
    ["8*8 display_Matrix", "1"],
    ["8*16 display_Matrix", "0"],
];

//执行器_点阵屏显示_左右对称
export const MAX_88display_Matrix_Rotation = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOW)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_Scroll_direction)
            .appendField(new Blockly.FieldDropdown(MAX_88display_Rotation_NUM), "Rotation_TYPE");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        //   this.setTooltip("");
    }
};




//执行器_点阵屏显示_字符显示
export const MAX_88display_Matrix_TEXT = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOW)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendValueInput("TEXT", String)
            .setCheck([Number, String])
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("display");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        //this.setTooltip("");
    }
};


//执行器_点阵屏显示_显示图案
export const MAX_88display_Matrix_DisplayChar = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOW)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendValueInput("LEDArrayX")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_PICARRAY);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        //this.setTooltip();
    }
};
export const MAX_816display_Matrix_DisplayChar = MAX_88display_Matrix_DisplayChar;






//执行器_点阵屏显示_图案数组
export const MAX_816display_Matrix_LedArray = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_ARRAYVAR)
            .appendField(new Blockly.FieldTextInput("LedArray0816"), "VAR");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a81")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a82")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a83")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a84")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a85")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a86")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a87")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a88")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a89")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a810")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a811")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a812")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a813")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a814")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a815")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a816");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a71")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a72")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a73")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a74")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a75")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a76")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a77")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a78")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a79")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a710")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a711")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a712")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a713")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a714")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a715")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a716");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a61")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a62")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a63")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a64")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a65")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a66")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a67")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a68")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a69")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a610")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a611")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a612")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a613")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a614")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a615")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a616");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a51")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a52")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a53")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a54")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a55")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a56")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a57")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a58")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a59")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a510")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a511")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a512")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a513")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a514")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a515")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a516");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a41")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a42")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a43")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a44")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a45")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a46")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a47")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a48")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a49")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a410")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a411")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a412")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a413")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a414")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a415")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a416");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a31")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a32")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a33")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a34")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a35")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a36")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a37")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a38")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a39")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a310")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a311")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a312")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a313")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a314")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a315")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a316");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a21")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a22")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a23")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a24")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a25")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a26")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a27")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a28")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a29")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a210")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a211")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a212")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a213")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a214")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a215")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a216");
        this.appendDummyInput("")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a11")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a12")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a13")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a14")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a15")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a16")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a17")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a18")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a19")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a110")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a111")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a112")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a113")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a114")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a115")
            .appendField(new Blockly.FieldCheckbox("FALSE"), "a116");
        this.setOutput(true, Number);
        this.setTooltip();

    }
};

//显示预设图案

export const MAX_816display_Matrix_Ledimg = {
    init: function () {
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_Preset_photo)
            .appendField(new Blockly.FieldDropdown([
                ["❤", "0100038007c00fe01ff01ff00ee00640"],
                ["♥", "00000100038007c00fe00ee004400000"],
                ["▲", "00003ffc1ff80ff007e003c001800000"],
                ["▼", "0000018003c007e00ff01ff83ffc0000"],
                ["◄", "100030007000f000f000700030001000"],
                ["►", "0008000c000e000f000f000e000c0008"],
                ["↑", "01800180018009900db007e003c00180"],
                ["↓", "018003c007e00db00990018001800180"],
                ["←", "180030006000ff00ff00600030001800"],
                ["→", "0018000c000600ff00ff0006000c0018"],
                ["☺", "000003c004200000000022441c380000"],
                ["☹", "0000042003c0000000001c3822440000"],
                ["♀♂", "10707cd8108838d8447944054403380f"],
                ["♪♪", "6060f8f87878080808080b0b0e0e0c0c"],
                ["囧", "0ff00a500a500bd00c300a500a500ff0"]
            ]), "img_")
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.setOutput(true);
        this.setTooltip('');
        this.setColour(MAX_HUE);

    }
};

//执行器_点阵屏显示_清除屏幕
export const MAX_88display_Matrix_CLEAR = {
    init: function () {
        this.setColour(MAX_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('myMatrix'), 'matrixName')
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_SHOW)
            .appendField(new Blockly.FieldImage(require("../media/matrix.png"), 100, 50));
        this.appendDummyInput("").setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_CLEAR);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        //this.setTooltip();
    }
};