import * as Blockly from 'blockly/core';

//LED模块
export const MAX_led_R = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dropdown_stat = this.getFieldValue('STAT');
    var code = "";
    if (window.isNaN(dropdown_pin)) {
        code = code + 'pinMode(' + dropdown_pin + ', OUTPUT);\n';
    } else {
        generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    }
    code += 'digitalWrite(' + dropdown_pin + ',' + dropdown_stat + ');\n'
    return code;
};

// export const MAX_led_R = MAX_led_Y;
// export const MAX_led_R = MAX_led_G;

//按键模块
export const MAX_button = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var code = "";
    if (window.isNaN(dropdown_pin)) {
        var funcName = 'mixly_digitalRead';
        var code2 = 'int' + ' ' + funcName + '(uint8_t pin) {\n'
            + '  pinMode(pin, INPUT);\n'
            + '  return digitalRead(pin);\n'
            + '}\n';
        generator.definitions_[funcName] = code2;
        code = 'mixly_digitalRead(' + dropdown_pin + ')';
    } else {
        if (generator.setups_['setup_output_' + dropdown_pin]) {
            //存在pinMode已设为output则不再设为input
        } else {
            generator.setups_['setup_input_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', INPUT);';
        }
        code = 'digitalRead(' + dropdown_pin + ')';
    }
    return [code, generator.ORDER_ATOMIC];
};



//声音模块
export const MAX_sound = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var code = 'analogRead(' + dropdown_pin + ')';
    return [code, generator.ORDER_ATOMIC];
};

export const MAX_light = MAX_sound;
//光线模块


export const ke_w_buzzer2 = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var fre = generator.valueToCode(this, 'FREQUENCY',
        generator.ORDER_ASSIGNMENT) || '0';
    var code = "tone(" + dropdown_pin + "," + fre + ");\n";
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    return code;
};

export const ke_w_buzzer3 = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var fre = generator.valueToCode(this, 'FREQUENCY',
        generator.ORDER_ASSIGNMENT) || '0';
    var dur = generator.valueToCode(this, 'DURATION',
        generator.ORDER_ASSIGNMENT) || '0';
    var code = "tone(" + dropdown_pin + "," + fre + "," + dur + ");\n";
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    return code;
};

////////////////////蜂鸣器//////////////////////
export const ke_tone01 = function (_, generator) {
    var code = this.getFieldValue('STAT');
    return [code, generator.ORDER_ATOMIC];
};

export const ke_buzzer = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var fre = generator.valueToCode(this, 'FREQUENCY',
        generator.ORDER_ASSIGNMENT) || '0';
    generator.setups_['setup_output'] = 'pinMode(' + dropdown_pin + ', OUTPUT);';

    var dropdown_type = this.getFieldValue('beat1');

    var code = 'tone(' + dropdown_pin + ',' + fre + ');\ndelay(' + dropdown_type + ');\n';
    /*if(window.isNaN(dropdown_pin)){
       code = code+'pinMode('+dropdown_pin+', OUTPUT);\n';
    }else{
       generator.setups_['setup_output_'+dropdown_pin] = 'pinMode('+dropdown_pin+', OUTPUT);';
    }*/
    //code += "tone("+dropdown_pin+","+fre+");\n";
    return code;
};

export const ke_controls_tone2 = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var fre = generator.valueToCode(this, 'FREQUENCY',
        generator.ORDER_ASSIGNMENT) || '0';
    var dur = generator.valueToCode(this, 'DURATION',
        generator.ORDER_ASSIGNMENT) || '0';
    var code = "";
    if (window.isNaN(dropdown_pin)) {
        code = code + 'pinMode(' + dropdown_pin + ', OUTPUT);\n';
    } else {
        generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    }
    code += "tone(" + dropdown_pin + "," + fre + "," + dur + ");\n";
    return code;
};

//////////////////////////music///////////////////////////
export const ke_music = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.setups_['setup_output_music'] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    generator.definitions_['include_birthday'] = 'void birthday()\n{\n  tone(' + dropdown_pin + ',294);\n  delay(250);\n  tone(' + dropdown_pin + ',440);\n  delay(250);\n  tone(' + dropdown_pin + ',392);\n  delay(250);\n  tone(' + dropdown_pin + ',532);\n  delay(250);\n  tone(' + dropdown_pin + ',494);\n  delay(500);\n  tone(' + dropdown_pin + ',392);\n  delay(250);\n  tone(' + dropdown_pin + ',440);\n  delay(250);\n  tone(' + dropdown_pin + ',392);\n  delay(250);\n  tone(' + dropdown_pin + ',587);\n  delay(250);\n  tone(' + dropdown_pin + ',532);\n  delay(500);\n  tone(' + dropdown_pin + ',392);\n  delay(250);\n  tone(' + dropdown_pin + ',784);\n  delay(250);\n  tone(' + dropdown_pin + ',659);\n  delay(250);\n  tone(' + dropdown_pin + ',532);\n  delay(250);\n  tone(' + dropdown_pin + ',494);\n  delay(250);\n  tone(' + dropdown_pin + ',440);\n  delay(250);\n  tone(' + dropdown_pin + ',698);\n  delay(375);\n  tone(' + dropdown_pin + ',659);\n  delay(250);\n  tone(' + dropdown_pin + ',532);\n  delay(250);\n  tone(' + dropdown_pin + ',587);\n  delay(250);\n  tone(' + dropdown_pin + ',532);\n  delay(500);\n}\n';
    generator.definitions_['include_tone'] = '//tone\n#define D0 -1\n#define D1 262\n#define D2 293\n#define D3 329\n#define D4 349\n#define D5 392\n#define D6 440\n#define D7 494\n#define M1 523\n#define M2 586\n#define M3 658\n#define M4 697\n#define M5 783\n#define M6 879\n#define M7 987\n#define H1 1045\n#define H2 1171\n#define H3 1316\n#define H4 1393\n#define H5 1563\n#define H6 1755\n#define H7 1971\n\n#define WHOLE 1\n#define HALF 0.5\n#define QUARTER 0.25\n#define EIGHTH 0.25\n#define SIXTEENTH 0.625\n ';

    generator.definitions_['include_tune'] = '\nint tune[]= \n{\n  M3,M3,M4,M5,\n  M5,M4,M3,M2,\n  M1,M1,M2,M3,\n  M3,M2,M2,\n  M3,M3,M4,M5,\n  M5,M4,M3,M2,\n  M1,M1,M2,M3,\n  M2,M1,M1,\n  M2,M2,M3,M1,\n  M2,M3,M4,M3,M1,\n  M2,M3,M4,M3,M2,\n  M1,M2,D5,D0,\n  M3,M3,M4,M5,\n  M5,M4,M3,M4,M2,\n  M1,M1,M2,M3,\n  M2,M1,M1\n};';
    generator.definitions_['include_durt'] = '\nfloat durt[]= \n {\n  1,1,1,1,\n  1,1,1,1,\n  1,1,1,1,\n  1+0.5,0.5,1+1,\n  1,1,1,1,\n  1,1,1,1,\n  1,1,1,1,\n  1+0.5,0.5,1+1,\n  1,1,1,1,\n  1,0.5,0.5,1,1,\n  1,0.5,0.5,1,1,\n  1,1,1,1,\n  1,1,1,1,\n  1,1,1,0.5,0.5,\n  1,1,1,1,\n  1+0.5,0.5,1+1,\n };';
    generator.definitions_['include_io1'] = '\n int length;\n int tonepin=' + dropdown_pin + '; \n';
    generator.definitions_['include_Ode_to_Joy'] = 'void Ode_to_Joy()\n{\n  for(int x=0;x<length;x++)\n  {\n    tone(tonepin,tune[x]);\n    delay(300*durt[x]);   \n  }\n}\n';

    generator.setups_['setup_output_Ode'] = 'length=sizeof(tune)/sizeof(tune[0]);\n';
    var dropdown_type = this.getFieldValue('play');

    var code = '';
    if (dropdown_type == "Birthday") code += 'birthday();\n';
    if (dropdown_type == "Ode to Joy") code += 'Ode_to_Joy();\n';
    //if (dropdown_type == "City of Sky") code += 'digitalRead(8)';
    return code;
    //return [code, generator.ORDER_ATOMIC];
};

export const ke_notone = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.setups_['setup_output'] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    var code = '';
    code += "noTone(" + dropdown_pin + ");\n";
    return code;
};

//RGB彩灯模块（含灯带，点阵，环形灯等）
export const MAX_rgb_init = function (_, generator) {
    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var value_ledcount = generator.valueToCode(this, 'LEDCOUNT', generator.ORDER_ATOMIC);

    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(' + value_ledcount + ',' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
    generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
    //generator.setups_['setup_rgb_display_setpin' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setPin(' + dropdown_rgbpin + ');';
    return '';
};
export const MAX_rgb_brightness = function (_, generator) {
    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var value_brightness = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    var code = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');';
    return code;
};

export const MAX_rgb = function (_, generator) {
    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var value__led_ = generator.valueToCode(this, '_LED_', generator.ORDER_ATOMIC);
    var value_rvalue = generator.valueToCode(this, 'RVALUE', generator.ORDER_ATOMIC);
    var value_gvalue = generator.valueToCode(this, 'GVALUE', generator.ORDER_ATOMIC);
    var value_bvalue = generator.valueToCode(this, 'BVALUE', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    generator.definitions_['var_rgb_MAX' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_MAX_' + dropdown_rgbpin + '(4,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
    generator.setups_['setup_rgb_MAX_begin_' + dropdown_rgbpin] = 'rgb_MAX_' + dropdown_rgbpin + '.begin();';
    //generator.setups_['setup_rgb_MAX_setpin'+dropdown_rgbpin] ='rgb_MAX_'+dropdown_rgbpin+'.setPin('+dropdown_rgbpin+');';

    var code = 'rgb_MAX_' + dropdown_rgbpin + '.setPixelColor(' + value__led_ + '-1, ' + value_rvalue + ',' + value_gvalue + ',' + value_bvalue + ');\n';
    code += 'rgb_MAX_' + dropdown_rgbpin + '.show();\n';
    return code;
};

export const MAX_rgb2 = function (_, generator) {
    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var value_led = generator.valueToCode(this, '_LED_', generator.ORDER_ATOMIC);
    var color = Blockly.utils.colour.hexToRgb(this.getFieldValue('RGB_LED_COLOR'));
    //generator.definitions_['include_Wire'] = '#include <Wire.h>';
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(4,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';

    }
    var code = 'rgb_display_' + dropdown_rgbpin + '.setPixelColor(' + value_led + '-1, ' + color + ');\n';
    code += 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    return code;
};

export const MAX_rgb3 = function (_, generator) {
    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var wait_time = generator.valueToCode(this, 'WAIT', generator.ORDER_ATOMIC);
    var value_brightness = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(25,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
        '(25,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_setBrightness' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');';
    }
    var funcName2 = 'Wheel';
    var code2 = 'uint32_t Wheel(byte WheelPos) {\n';
    code2 += 'if(WheelPos < 85) {return rgb_display_' + dropdown_rgbpin + '.Color(WheelPos * 3, 255 - WheelPos * 3, 0);} \n';
    code2 += 'else if(WheelPos < 170) {WheelPos -= 85; return rgb_display_' + dropdown_rgbpin + '.Color(255 - WheelPos * 3, 0, WheelPos * 3);}\n ';
    code2 += 'else {WheelPos -= 170;return rgb_display_' + dropdown_rgbpin + '.Color(0, WheelPos * 3, 255 - WheelPos * 3);}\n';
    code2 += '}\n';
    generator.definitions_[funcName2] = code2;

    var funcName3 = 'rainbow';
    var code3 = 'void rainbow(uint8_t wait) { uint16_t i, j;\n';
    code3 += '      for(j=0; j<256; j++) {               \n';
    code3 += '        for(i=0; i<rgb_display_' + dropdown_rgbpin + '.numPixels(); i++) {\n';
    code3 += '         rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, Wheel((i+j) & 255));}\n';
    code3 += '      rgb_display_' + dropdown_rgbpin + '.show();\n';
    code3 += '      delay(wait);}}\n';

    generator.definitions_[funcName3] = code3;

    var code = 'rainbow(' + wait_time + ');\n'
    return code;
};

export const MAX_rgb4 = function (_, generator) {
    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var wait_time = generator.valueToCode(this, 'WAIT', generator.ORDER_ATOMIC);
    var value_brightness = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(25,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';

        generator.setups_['setup_rgb_display_setBrightness' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');';
    }
    var funcName2 = 'Wheel';
    var code2 = 'uint32_t Wheel(byte WheelPos) {\n';
    code2 += 'if(WheelPos < 85) {return rgb_display_' + dropdown_rgbpin + '.Color(WheelPos * 3, 255 - WheelPos * 3, 0);} \n';
    code2 += 'else if(WheelPos < 170) {WheelPos -= 85; return rgb_display_' + dropdown_rgbpin + '.Color(255 - WheelPos * 3, 0, WheelPos * 3);}\n ';
    code2 += 'else {WheelPos -= 170;return rgb_display_' + dropdown_rgbpin + '.Color(0, WheelPos * 3, 255 - WheelPos * 3);}\n';
    code2 += '}\n';
    generator.definitions_[funcName2] = code2;

    var funcName3 = 'rainbow';
    var code3 = 'void rainbow(uint8_t wait) { uint16_t i, j;\n';
    code3 += '      for(j=0; j<256; j++) {               \n';
    code3 += '        for(i=0; i<rgb_display_' + dropdown_rgbpin + '.numPixels(); i++) {\n';
    code3 += '         rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, Wheel((i+j) & 255));}\n';
    code3 += '      rgb_display_' + dropdown_rgbpin + '.show();\n';
    code3 += '      delay(wait);}}\n';

    generator.definitions_[funcName3] = code3;

    var funcName4 = 'rainbowCycle';
    var code4 = 'void rainbowCycle(uint8_t wait) {uint16_t i, j;\n';
    code4 += '    for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel\n';
    code4 += '      for(i=0; i< rgb_display_' + dropdown_rgbpin + '.numPixels(); i++) {\n';
    code4 += '        rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, Wheel(((i * 256 / rgb_display_' + dropdown_rgbpin + '.numPixels()) + j) & 255));}\n';
    code4 += '     rgb_display_' + dropdown_rgbpin + '.show();\n';
    code4 += '     delay(wait);}}\n';

    generator.definitions_[funcName4] = code4;

    var code = 'rainbowCycle(' + wait_time + ');\n'
    return code;
};


export const MAX_display_Matrix_DisplayChar = function (_, generator) {

    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dotMatrixArray = generator.valueToCode(this, 'RGBArray', generator.ORDER_ASSIGNMENT);
    var value_brightness = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    //generator.definitions_['RGBArray'] = 'uint8_t  RGBArray[16][3];';
    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(25,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
        generator.setups_['setup_rgb_display_setpin' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setPin(' + dropdown_rgbpin + ');';
        generator.setups_['setup_rgb_display_setBrightness' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');';
    }
    var code = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');\n';
    code += 'for(int i=0; i<25; i++)\n';
    code += '{\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, ' + dotMatrixArray + '[i][0],' + dotMatrixArray + '[i][1],' + dotMatrixArray + '[i][2]);\n';
    code += '}\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    code += 'delay(100);\n';
    return code;
};

export const MAX_display_Matrix_LedArray = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    var code = '{';
    for (var i = 1; i < 6; i++) {
        for (var j = 1; j < 6; j++) {
            code += '\n          {';
            code += Blockly.utils.colour.hexToRgb(this.getFieldValue('RGB_' + i + j));
            code += '}';
            if (i != 5 || j != 5) code += ','
        }
    }
    code += '};';
    generator.definitions_[varName] = "uint8_t  " + varName + "[25][3]=" + code;
    return [varName, generator.ORDER_ATOMIC];
}
export const MAX_display_Matrix_DisplayLedBar = function (_, generator) {

    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dotMatrixArray = generator.valueToCode(this, 'RGBArray', generator.ORDER_ASSIGNMENT);
    var value_brightness = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';
    //generator.definitions_['RGBArray'] = 'uint8_t  RGBArray[16][3];';
    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(5,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
        generator.setups_['setup_rgb_display_setpin' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setPin(' + dropdown_rgbpin + ');';
        generator.setups_['setup_rgb_display_setBrightness' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');';
    }
    var code = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');\n';
    code += 'for(int i=0; i<5; i++)\n';
    code += '{\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, ' + dotMatrixArray + '[i][0],' + dotMatrixArray + '[i][1],' + dotMatrixArray + '[i][2]);\n';
    code += '}\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    code += 'delay(100);\n';
    return code;
};
export const MAX_display_Matrix_LedBar = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    var code = '{';
    for (var j = 1; j < 6; j++) {
        code += '\n          {';
        code += Blockly.utils.colour.hexToRgb(this.getFieldValue('RGB_' + j));
        code += '}';
        if (j != 5) code += ','
    }
    code += '};';
    generator.definitions_[varName] = "uint8_t  " + varName + "[5][3]=" + code;
    return [varName, generator.ORDER_ATOMIC];
}

export const MAX_display_Matrix_DisplayChar2 = function (_, generator) {

    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dotMatrixArray = generator.valueToCode(this, 'RGBArray_dot', generator.ORDER_ASSIGNMENT);
    var value_brightness = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    var color = Blockly.utils.colour.hexToRgb(this.getFieldValue('RGB_DOT_COLOR'));

    // generator.definitions_['rgb_color'] = "uint8_t  rgb_color[3]={" + color+'}\n;';


    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';

    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(25,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
        generator.setups_['setup_rgb_display_setpin' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setPin(' + dropdown_rgbpin + ');';
        generator.setups_['setup_rgb_display_setBrightness' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');';
    }
    var code = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');\n';
    code += 'for(int i=0; i<25; i++)\n';
    code += '{\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, ' + dotMatrixArray + '[i]*' + color[0] + ',' + dotMatrixArray + '[i]*' + color[1] + ',' + dotMatrixArray + '[i]*' + color[2] + ');\n';
    code += '}\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    code += 'delay(100);\n';
    return code;
};

export const MAX_display_Matrix_LedArray2 = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    //var RGB_ = new Array();
    var code = '{';
    for (var i = 1; i < 6; i++) {
        for (var j = 1; j < 6; j++) {
            code += '';
            code += (this.getFieldValue('DOT_' + i + j) == "TRUE") ? 1 : 0;
            code += '';
            if (i != 5 || j != 5) code += ','
        }
    }
    code += '};';

    //generator.definitions_[this.id] = "byte LedArray_"+clearString(this.id)+"[]="+code;
    generator.definitions_[varName] = "uint8_t  " + varName + "[25]=" + code;
    //return ["LedArray_"+clearString(this.id), generator.ORDER_ATOMIC];
    return [varName, generator.ORDER_ATOMIC];
}
export const MAX_display_Matrix_DisplayBar2 = function (_, generator) {

    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dotMatrixArray = generator.valueToCode(this, 'RGBArray_dot', generator.ORDER_ASSIGNMENT);
    var value_brightness = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    var color = Blockly.utils.colour.hexToRgb(this.getFieldValue('RGB_DOT_COLOR'));

    // generator.definitions_['rgb_color'] = "uint8_t  rgb_color[3]={" + color+'}\n;';


    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';

    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(5,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
        generator.setups_['setup_rgb_display_setpin' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setPin(' + dropdown_rgbpin + ');';
        generator.setups_['setup_rgb_display_setBrightness' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setBrightness(' + value_brightness + ');';
    }
    var code = '';
    code += 'for(int i=0; i<5; i++)\n';
    code += '{\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, ' + dotMatrixArray + '[i]*' + color[0] + ',' + dotMatrixArray + '[i]*' + color[1] + ',' + dotMatrixArray + '[i]*' + color[2] + ');\n';
    code += '}\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    code += 'delay(100);\n';
    return code;
};

export const MAX_display_Matrix_LedBar2 = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    //var RGB_ = new Array();
    var code = '{';

    for (var j = 1; j < 6; j++) {
        code += '';
        code += (this.getFieldValue('DOT_' + j) == "TRUE") ? 1 : 0;
        code += '';
        if (j != 5) code += ','
    }
    code += '};';

    //generator.definitions_[this.id] = "byte LedArray_"+clearString(this.id)+"[]="+code;
    generator.definitions_[varName] = "uint8_t  " + varName + "[5]=" + code;
    //return ["LedArray_"+clearString(this.id), generator.ORDER_ATOMIC];
    return [varName, generator.ORDER_ATOMIC];
}

export const MAX_display_Matrix_CLEAR = function (_, generator) {

    var dropdown_rgbpin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.definitions_['include_Adafruit_NeoPixel'] = '#include <Adafruit_NeoPixel.h>';

    if (!generator.definitions_['var_rgb_display' + dropdown_rgbpin]) {
        generator.definitions_['var_rgb_display' + dropdown_rgbpin] = 'Adafruit_NeoPixel  rgb_display_' + dropdown_rgbpin + '(25,' + dropdown_rgbpin + ', NEO_GRB + NEO_KHZ800);';
        generator.setups_['setup_rgb_display_begin_' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.begin();';
        generator.setups_['setup_rgb_display_setpin' + dropdown_rgbpin] = 'rgb_display_' + dropdown_rgbpin + '.setPin(' + dropdown_rgbpin + ');';
    }
    var code = '';
    code += 'for(int i=0; i<25; i++)\n';
    code += '{\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.setPixelColor(i, 0,0,0);\n';
    code += '}\n'
    code += 'rgb_display_' + dropdown_rgbpin + '.show();\n';
    code += 'delay(100);\n';
    return code;
};


export const MAX_88display_Matrix_blink_rate = function (_, generator) {
    var matrixName = this.getFieldValue('matrixName');
    var BLINK_RATE = generator.valueToCode(this, 'Blink_rate', generator.ORDER_ATOMIC);
    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';
    var code = '';
    code += matrixName + '.blinkRate(' + BLINK_RATE + ');\n';
    return code;
};
//辅助块_点阵屏_清除显示
export const MAX_88display_Matrix_brightness = function (_, generator) {
    var matrixName = this.getFieldValue('matrixName');
    var BRIGHTNESS = generator.valueToCode(this, 'Brightness', generator.ORDER_ATOMIC);
    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';
    var code = '';
    code += matrixName + '.setBrightness(' + BRIGHTNESS + ');\n';
    return code;
};
export const MAX_88display_Matrix_POS = function (_, generator) {
    var pos_x = generator.valueToCode(this, 'XVALUE', generator.ORDER_ASSIGNMENT);
    var pos_y = generator.valueToCode(this, 'YVALUE', generator.ORDER_ASSIGNMENT);
    var matrixName = this.getFieldValue('matrixName');
    var dropdown_type = this.getFieldValue('DrawPixel_TYPE');

    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';

    var code = matrixName + '.drawPixel(' + pos_x + '-1,' + pos_y + '-1,' + dropdown_type + ');\n'
    code += matrixName + '.writeDisplay();\n';
    return code;
};

export const MAX_88display_Matrix_drawLine = function (_, generator) {

    var pos_x0 = generator.valueToCode(this, 'X0VALUE', generator.ORDER_ASSIGNMENT);
    var pos_y0 = generator.valueToCode(this, 'Y0VALUE', generator.ORDER_ASSIGNMENT);
    var pos_x1 = generator.valueToCode(this, 'X1VALUE', generator.ORDER_ASSIGNMENT);
    var pos_y1 = generator.valueToCode(this, 'Y1VALUE', generator.ORDER_ASSIGNMENT);
    var matrixName = this.getFieldValue('matrixName');
    var dropdown_type = this.getFieldValue('DrawPixel_TYPE');

    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';

    var code = matrixName + '.drawLine(' + pos_x0 + '-1,' + pos_y0 + '-1,' + pos_x1 + '-1,' + pos_y1 + '-1,' + dropdown_type + ');\n'
    code += matrixName + '.writeDisplay();\n';

    return code;
};

export const MAX_88display_Matrix_drawCircle = function (_, generator) {
    var drawCircle_type = this.getFieldValue('drawCircle_TYPE');
    var pos_x = generator.valueToCode(this, 'XVALUE', generator.ORDER_ASSIGNMENT);
    var pos_y = generator.valueToCode(this, 'YVALUE', generator.ORDER_ASSIGNMENT);
    var pos_r = generator.valueToCode(this, 'RVALUE', generator.ORDER_ASSIGNMENT);
    var matrixName = this.getFieldValue('matrixName');
    var dropdown_type = this.getFieldValue('DrawPixel_TYPE');

    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';
    var code = '';
    if (drawCircle_type == 0) {
        code = matrixName + '.drawCircle(' + pos_x + '-1,' + pos_y + '-1,' + pos_r + ',' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    } else {
        code = matrixName + '.fillCircle(' + pos_x + '-1,' + pos_y + '-1,' + pos_r + ',' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    }

    return code;
};

export const MAX_88display_Matrix_drawRect = function (_, generator) {
    var drawCircle_type = this.getFieldValue('drawRect_TYPE');
    var pos_x = generator.valueToCode(this, 'XVALUE', generator.ORDER_ASSIGNMENT);
    var pos_y = generator.valueToCode(this, 'YVALUE', generator.ORDER_ASSIGNMENT);
    var pos_w = generator.valueToCode(this, 'WVALUE', generator.ORDER_ASSIGNMENT);
    var pos_h = generator.valueToCode(this, 'HVALUE', generator.ORDER_ASSIGNMENT);
    var matrixName = this.getFieldValue('matrixName');
    var dropdown_type = this.getFieldValue('DrawPixel_TYPE');

    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';
    var code = '';
    if (drawCircle_type == 0) {
        code = matrixName + '.drawRect(' + pos_x + '-1,' + pos_y + '-1,' + pos_w + ',' + pos_h + ',' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    } else {
        code = matrixName + '.fillRect(' + pos_x + '-1,' + pos_y + '-1,' + pos_w + ',' + pos_h + ',' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    }

    return code;
};

export const MAX_88display_Matrix_drawRoundRect = function (_, generator) {
    var drawCircle_type = this.getFieldValue('drawRoundRect_TYPE');
    var pos_x = generator.valueToCode(this, 'XVALUE', generator.ORDER_ASSIGNMENT);
    var pos_y = generator.valueToCode(this, 'YVALUE', generator.ORDER_ASSIGNMENT);
    var pos_w = generator.valueToCode(this, 'WVALUE', generator.ORDER_ASSIGNMENT);
    var pos_h = generator.valueToCode(this, 'HVALUE', generator.ORDER_ASSIGNMENT);
    var pos_r = generator.valueToCode(this, 'RVALUE', generator.ORDER_ASSIGNMENT);
    var matrixName = this.getFieldValue('matrixName');
    var dropdown_type = this.getFieldValue('DrawPixel_TYPE');

    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';
    var code = '';
    if (drawCircle_type == 0) {
        code = matrixName + '.drawRoundRect(' + pos_x + '-1,' + pos_y + '-1,' + pos_w + ',' + pos_h + ',' + pos_r + ',' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    } else {
        code = matrixName + '.fillRoundRect(' + pos_x + '-1,' + pos_y + '-1,' + pos_w + ',' + pos_h + ',' + pos_r + ',' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    }

    return code;
};


export const MAX_88display_Matrix_drawTriangle = function (_, generator) {
    var drawCircle_type = this.getFieldValue('drawRoundRect_TYPE');
    var pos_x0 = generator.valueToCode(this, 'X0VALUE', generator.ORDER_ASSIGNMENT);
    var pos_y0 = generator.valueToCode(this, 'Y0VALUE', generator.ORDER_ASSIGNMENT);
    var pos_x1 = generator.valueToCode(this, 'X1VALUE', generator.ORDER_ASSIGNMENT);
    var pos_y1 = generator.valueToCode(this, 'Y1VALUE', generator.ORDER_ASSIGNMENT);
    var pos_x2 = generator.valueToCode(this, 'X2VALUE', generator.ORDER_ASSIGNMENT);
    var pos_y2 = generator.valueToCode(this, 'Y2VALUE', generator.ORDER_ASSIGNMENT);
    var matrixName = this.getFieldValue('matrixName');
    var dropdown_type = this.getFieldValue('DrawPixel_TYPE');

    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';
    var code = '';
    if (drawCircle_type == 0) {
        code = matrixName + '.drawTriangle(' + pos_x0 + '-1,' + pos_y0 + '-1,' + pos_x1 + '-1,' + pos_y1 + '-1,' + pos_x2 + '-1,' + pos_y2 + '-1,' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    } else {
        code = matrixName + '.fillTriangle(' + pos_x0 + '-1,' + pos_y0 + '-1,' + pos_x1 + '-1,' + pos_y1 + '-1,' + pos_x2 + '-1,' + pos_y2 + '-1,' + dropdown_type + ');\n'
        code += matrixName + '.writeDisplay();\n';
    }

    return code;
};


export const MAX_88display_Matrix_Rotation = function (_, generator) {
    var matrixName = this.getFieldValue('matrixName');
    var dropdown_type = this.getFieldValue('Rotation_TYPE');
    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';

    var code = matrixName + '.setHf_flip(' + dropdown_type + ');\n'
    return code;
};
export const MAX_88display_Matrix_TEXT = function (_, generator) {
    var matrixName = this.getFieldValue('matrixName');
    var textString = generator.valueToCode(this, 'TEXT', generator.ORDER_ASSIGNMENT);
    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';
    var code = matrixName + '.drawStr(' + textString + ');\n'
    return code;
};
//执行器_点阵屏显示_显示图案
export const MAX_88display_Matrix_DisplayChar = function (_, generator) {
    var matrixName = this.getFieldValue('matrixName');
    var dotMatrixArray = generator.valueToCode(this, 'LEDArrayX', generator.ORDER_ASSIGNMENT);
    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';


    generator.definitions_['LEDArray'] = 'uint8_t  LEDArray[8];';
    var code = '';
    code += 'for(int i=0; i<8; i++)\n';
    code += '{\n'
    code += '  LEDArray[i]=' + dotMatrixArray + '[i];\n';
    code += '  for(int j=0; j<8; j++)\n'
    code += '  {\n'
    code += '    if((LEDArray[i]&0x01)>0)\n';
    code += '    ' + matrixName + '.drawPixel(j, i,1);\n';
    code += '    LEDArray[i] = LEDArray[i]>>1;\n';
    code += '  }  \n'
    code += '}\n'
    code += matrixName + '.writeDisplay();\n'
    return code;
};

//执行器_点阵屏显示_显示图案
export const MAX_816display_Matrix_DisplayChar = function (_, generator) {
    var matrixName = this.getFieldValue('matrixName');
    var dotMatrixArray = generator.valueToCode(this, 'LEDArrayX', generator.ORDER_ASSIGNMENT);
    generator.definitions_['include_HT16K33'] = '#include <HT16K33.h>';
    generator.definitions_[matrixName] = 'HT16K33  ' + matrixName + '; ';
    generator.setups_['setup_' + matrixName] = matrixName + '.begin(0x70); \n';

    generator.definitions_['LEDArray'] = 'uint16_t  LEDArray[8];';
    var code = '';
    code += 'for(int i=0; i<8; i++)\n';
    code += '{\n'
    code += '  LEDArray[i]=' + dotMatrixArray + '[i];\n';
    code += '  for(int j=15; j>=0; j--)\n'
    code += '  {\n'
    code += '    if((LEDArray[i]&0x01)>0){\n';
    code += '    ' + matrixName + '.drawPixel(j,i,1); }\n';
    code += '    LEDArray[i] = LEDArray[i]>>1;\n';
    code += '  }  \n'
    code += '}\n'
    code += matrixName + '.writeDisplay();\n'
    return code;
};
//执行器_点阵屏显示_点阵数组
export const MAX_88display_Matrix_LedArray = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    var a = new Array();
    for (var i = 1; i < 9; i++) {
        a[i] = new Array();
        for (var j = 1; j < 9; j++) {
            a[i][j] = (this.getFieldValue('a' + i + j) == "TRUE") ? 1 : 0;
        }
    }
    var code = '{';
    for (let i = 1; i < 9; i++) {
        var tmp = ""
        for (let j = 1; j < 9; j++) {
            tmp += a[i][j];
        }
        tmp = (parseInt(tmp, 2)).toString(16)
        if (tmp.length == 1) tmp = "0" + tmp;
        code += '0x' + tmp + ((i != 8) ? ',' : '');
    }
    code += '};';
    //generator.definitions_[this.id] = "byte LedArray_"+clearString(this.id)+"[]="+code;
    generator.definitions_[varName] = "uint8_t " + varName + "[8]=" + code;
    //return ["LedArray_"+clearString(this.id), generator.ORDER_ATOMIC];
    return [varName, generator.ORDER_ATOMIC];
};
//显示--选择预设8*8图案
export const MAX_88display_Matrix_Ledimg = function (_, generator) {
    var dropdown_img_ = this.getFieldValue('img_');
    var code = '"' + dropdown_img_ + '"';
    code = '{';
    for (var i = 0; i < 15; i += 2) {
        code += '0x' + dropdown_img_.substr(i, 2) + ((i != 14) ? ',' : '');
    }
    code += '};\n';
    generator.definitions_['matrix_img_' + dropdown_img_] = "byte " + 'matrix_img_' + dropdown_img_ + "[]=" + code;
    return ['matrix_img_' + dropdown_img_, generator.ORDER_ATOMIC];
};
//执行器_点阵屏显示_图案数组
export const MAX_816display_Matrix_LedArray = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    var a = new Array();
    for (var i = 1; i < 9; i++) {
        a[i] = new Array();
        for (var j = 1; j < 17; j++) {
            a[i][j] = (this.getFieldValue('a' + i + j) == "TRUE") ? 1 : 0;
        }
    }
    var code = '{';
    for (let i = 1; i < 9; i++) {
        var tmp = ""
        for (let j = 1; j < 17; j++) {
            tmp += a[i][j];
        }
        tmp = (parseInt(tmp, 2)).toString(16)
        //  alert(tmp);
        if (tmp.length == 1)
            tmp = "000" + tmp;
        else if (tmp.length == 2)
            tmp = "00" + tmp;
        else if (tmp.length == 3)
            tmp = "0" + tmp;
        code += '0x' + tmp + ((i != 8) ? ',' : '');
    }
    code += '};';
    //generator.definitions_[this.id] = "byte LedArray_"+clearString(this.id)+"[]="+code;
    generator.definitions_[varName] = "uint16_t " + varName + "[8]=" + code;
    //return ["LedArray_"+clearString(this.id), generator.ORDER_ATOMIC];
    return [varName, generator.ORDER_ATOMIC];
};
//显示--选择预设8*16图案
export const MAX_816display_Matrix_Ledimg = function (_, generator) {
    var dropdown_img_ = this.getFieldValue('img_');
    var code = '"' + dropdown_img_ + '"';
    code = '{';
    for (var i = 0; i < 29; i += 4) {
        code += '0x' + dropdown_img_.substr(i, 4) + ((i != 28) ? ',' : '');
    }
    code += '};\n';
    generator.definitions_['matrix_img_' + dropdown_img_] = "uint16_t  " + 'matrix_img_' + dropdown_img_ + "[]=" + code;
    return ['matrix_img_' + dropdown_img_, generator.ORDER_ATOMIC];
};
//清屏
export const MAX_88display_Matrix_CLEAR = function () {
    var matrixName = this.getFieldValue('matrixName');
    var code = matrixName + '.clear();\n'
    code += matrixName + '.writeDisplay();\n';
    return code;
};