export const EnMsg = {};

EnMsg.MIXLY_MAX_LED_R='red LED';
EnMsg.MIXLY_MAX_LED_Y='yellow LED';
EnMsg.MIXLY_MAX_LED_G='green LED';

EnMsg.MIXLY_MAX_BUTTON='button';
EnMsg.MIXLY_MAX_SOUND='sound';
EnMsg.MIXLY_MAX_LIGHT='light';


EnMsg.MIXLY_ke_BUZZER2='Passive Buzzer';
EnMsg.kids_Ode_to_joy = "Ode_to_joy";
EnMsg.kids_birthday = "birthday";

EnMsg.kids_tone = "tone";
EnMsg.kids_beat = "beat";
EnMsg.kids_play_tone = "play_tone";
EnMsg.kids_notone = "no_tone";

EnMsg.MIXLY_brightness='brightness';
EnMsg.MIXLY_init_brightness='Init brightness';
EnMsg.MIXLY_TIME_FOR_CHANGE_COLOUR='time for change colour';
EnMsg.MIXLY_TIME_FOR_LOOP_COLOUR='time for colour loop';
EnMsg.MIXLY_drawLine='Dot Matrix drawLine';
EnMsg.MIXLY_drawdrawCircle='Dot Matrix drawCircle';
EnMsg.MIXLY_drawRect='Dot Matrix drawRect';
EnMsg.MIXLY_drawRoundRect='Dot Matrix drawRoundRect';
EnMsg.MIXLY_drawTriangle='Dot Matrix drawTriangle';
EnMsg.MIXLY_Scroll_direction='Dot Matrix text scroll direction';
EnMsg.MIXLY_Preset_photo='Dot Matrix preset photo';


export const EnCatgories = {};