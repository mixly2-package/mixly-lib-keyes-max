export const ZhHansMsg = {};

ZhHansMsg.MIXLY_MAX_LED_R='红色 LED';
ZhHansMsg.MIXLY_MAX_LED_Y='黄色 LED';
ZhHansMsg.MIXLY_MAX_LED_G='绿色 LED';

ZhHansMsg.MIXLY_MAX_BUTTON='按键';
ZhHansMsg.MIXLY_MAX_SOUND='声音传感器';
ZhHansMsg.MIXLY_MAX_LIGHT='光线传感器';

ZhHansMsg.MIXLY_ke_BUZZER2='无源蜂鸣器';
ZhHansMsg.kids_Ode_to_joy = "圣诞歌";
ZhHansMsg.kids_birthday = "生日快乐";

ZhHansMsg.kids_tone = "音调";
ZhHansMsg.kids_beat = "节拍";
ZhHansMsg.kids_play_tone = "播放乐曲";
ZhHansMsg.kids_notone = "关闭蜂鸣器";

ZhHansMsg.MIXLY_brightness='亮度';
ZhHansMsg.MIXLY_init_brightness='初始亮度';
ZhHansMsg.MIXLY_TIME_FOR_CHANGE_COLOUR='七彩变换切换时间';
ZhHansMsg.MIXLY_TIME_FOR_LOOP_COLOUR='七彩循环切换时间';
ZhHansMsg.MIXLY_drawLine='点阵画直线';
ZhHansMsg.MIXLY_drawdrawCircle='点阵画圆';
ZhHansMsg.MIXLY_drawRect='点阵画矩形';
ZhHansMsg.MIXLY_drawRoundRect='点阵画圆角矩形';
ZhHansMsg.MIXLY_drawTriangle='点阵画三角形';
ZhHansMsg.MIXLY_Scroll_direction='点阵屏滚动文字方向';
ZhHansMsg.MIXLY_Preset_photo='点阵预设图案';

export const ZhHansCatgories = {};