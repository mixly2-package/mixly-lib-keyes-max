export const ZhHantMsg = {};

ZhHantMsg.MIXLY_MAX_LED_R='红色 LED';
ZhHantMsg.MIXLY_MAX_LED_Y='黄色 LED';
ZhHantMsg.MIXLY_MAX_LED_G='绿色 LED';

ZhHantMsg.MIXLY_MAX_BUTTON='按键';
ZhHantMsg.MIXLY_MAX_SOUND='声音传感器';
ZhHantMsg.MIXLY_MAX_LIGHT='光线传感器';

ZhHantMsg.MIXLY_ke_BUZZER2='無源蜂鳴器';
ZhHantMsg.kids_Ode_to_joy = "圣诞歌";
ZhHantMsg.kids_birthday = "生日快乐";



ZhHantMsg.kids_tone = "音调";
ZhHantMsg.kids_beat = "节拍";
ZhHantMsg.kids_play_tone = "播放乐曲";
ZhHantMsg.kids_notone = "关闭蜂鸣器";

ZhHantMsg.MIXLY_brightness='亮度';
ZhHantMsg.MIXLY_init_brightness='初始亮度';
ZhHantMsg.MIXLY_TIME_FOR_CHANGE_COLOUR='七彩变换切换时间';
ZhHantMsg.MIXLY_TIME_FOR_LOOP_COLOUR='七彩循环切换时间';
ZhHantMsg.MIXLY_drawLine='点阵画直线 ';
ZhHantMsg.MIXLY_drawdrawCircle='点阵画圆';
ZhHantMsg.MIXLY_drawRect='点阵画矩形';
ZhHantMsg.MIXLY_drawRoundRect='点阵画圆角矩形';
ZhHantMsg.MIXLY_drawTriangle='点阵画三角形';
ZhHantMsg.MIXLY_Scroll_direction='点阵屏滚动文字方向';
ZhHantMsg.MIXLY_Preset_photo='点阵预设图案';

export const ZhHantCatgories = {};